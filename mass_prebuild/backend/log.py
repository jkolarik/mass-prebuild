""" Logging facility

    This module provides logging facilities to MPB
"""

import logging
from pathlib import Path
from os import getpid


class MpbLog:
    """Mass pre-build logging facility

        This class implements logging for MPB. By default logs are printed to the console, unless a
        log level is specified, in which case they may be printed only to a rotating log file.

        Basically, the mapping is the following:
        - No level: Not printed
        - Debug/Info/Warning/Error: Console + rotating log file
        - Critical: Rotating log file only

        Logs for which the level is set may be filtered out depending on the MPB Verbosity level.
        If no log level is to be set, print() must be used instead.
    """

    def __init__(self, name='mpb', verbose=0):
        """Create a new logger

        Args:
            name: Name of the logger to create
            verbose: level of verbosity, which is translated to log level
        """
        self._level = logging.ERROR - (verbose * 10)

        home_path = Path(Path.home() / '.mpb')
        home_path.mkdir(parents=True, exist_ok=True)

        self.logger = logging.getLogger(f'{name}.{getpid()}')
        self.logger.setLevel(logging.DEBUG)

        self.stream_handler = logging.StreamHandler()
        formatter = logging.Formatter('%(message)s')
        self.stream_handler.setFormatter(formatter)
        self.stream_handler.setLevel(self._level)

        self.file_handler = logging.handlers.RotatingFileHandler(
                str(Path(home_path / f'{name}.log')),
                maxBytes=10000000,
                backupCount=2,
                encoding='utf-8'
                )
        formatter = logging.Formatter(fmt='%(asctime)s %(name)s %(levelname)s %(message)s',
                                      datefmt='%x %X')
        self.file_handler.setFormatter(formatter)
        self.file_handler.setLevel(logging.DEBUG)

        self.logger.addHandler(self.file_handler)
        self.logger.addHandler(self.stream_handler)

    @property
    def level(self):
        """Returns the log level"""
        return self._level

    @level.setter
    def level(self, val):
        """Set the log level"""
        if any([val > 5, val < -2]):
            raise ValueError("Verbose value must be between -1 and 5")

        self._level = logging.ERROR - (val * 10)
        self.stream_handler.setLevel(self._level)

    def verbosity(self):
        """Returns the log level converted to verbosity"""
        return (logging.ERROR - self.level) / 10

    def log(self, level, msg):
        """Log message into the logger and the console depending on the level"""
        self.logger.log(level, msg)

    def debug(self, msg):
        """Log a message"""
        self.log(logging.DEBUG, msg)

    def info(self, msg):
        """Log a message"""
        self.log(logging.INFO, msg)

    def warning(self, msg):
        """Log a message"""
        self.log(logging.WARNING, msg)

    def error(self, msg):
        """Log a message"""
        self.log(logging.ERROR, msg)

    def critical(self, msg):
        """Log a message"""
        self.log(logging.CRITICAL, msg)
