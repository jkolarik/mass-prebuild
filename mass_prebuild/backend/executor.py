""" Command executor wrapper

    This module provides helpers for shell commands.
    Ultimately, it can be used to abstract command execution through SSH.
"""

import subprocess


class MpbExecutor:
    """
    An executor for running commands on remote systems using SSH or locally.

    Args:
        cwd (str): The current working directory if not executing remotely.
        remote (Optional[str]): A string of the form 'user@host' if executing
            remotely, else None.

    Attributes:
        _remote (Optional[str]): The host and user for remote execution
            in form 'user@host'.
        _cwd (str): The current working directory if not executing remotely.
    """

    def __init__(self, logger, ssh=None, scp=None, remote=None) -> None:
        """
        Initialize the MpbExecutor with a given current working directory and
        optional remote information.
        """
        self._remote = remote
        self._ssh = ssh
        self._scp = scp
        self._logger = logger

        if all([any([self._ssh is None, self._scp is None]), self._remote is not None]):
            raise ValueError("Remote is set but either ssh or scp commands aren't provided")

    def scp(self, src, dest) -> subprocess.CompletedProcess:
        """
        Execute the given command and return its output as a string.
        This method handles both local and remote execution by appending necessary
        prefixes to the command based on the provided parameters.
        """
        if self._remote is None:
            raise RuntimeError("Remote is not provided")

        cmd = [self._scp, '-r', f'{self._remote}:{src}', str(dest)]

        self._logger.debug(f'Running {cmd}')

        return subprocess.run(cmd,
                              stdout=subprocess.PIPE,
                              stderr=subprocess.STDOUT,
                              text=True,
                              check=False)

    def exec(self, cmd, cwd=None, local=False) -> subprocess.CompletedProcess:
        """
        Execute the given command and return its output as a string.
        This method handles both local and remote execution by appending necessary
        prefixes to the command based on the provided parameters.
        """
        if all([self._remote is not None, not local]):
            cmd_s = " ".join(cmd)
            if cwd is not None:
                cmd_s = f'cd {cwd}; {cmd_s}'
                cwd = None  # Don't confuse the run command
            cmd = [self._ssh, self._remote, cmd_s]

        self._logger.debug(f'Running {cmd}')
        return subprocess.run(cmd,
                              stdout=subprocess.PIPE,
                              stderr=subprocess.STDOUT,
                              cwd=cwd,
                              text=True,
                              check=False)

    def popen(self, cmd) -> subprocess.Popen:
        """
        Run the given command in a separate process and return its Popen object.
        Optionally, write its output to a specified file if provided.
        """
        # pylint: disable = consider-using-with
        # If stdout is directed to PIPE, applications may have troubles to save their output
        # e.g. `mock` fails to store its build logs properly.
        if self._remote is not None:
            cmd = [self._ssh, self._remote, " ".join(cmd)]

        self._logger.debug(f'Running {cmd}')
        result = subprocess.Popen(cmd,
                                  stdout=subprocess.DEVNULL,
                                  stderr=subprocess.STDOUT,
                                  text=True)

        return result

    def which(self, arg) -> str:
        """
        Run 'which' command with the specified argument and return its output as a string.
        """
        return self.exec(['which', arg]).stdout.rstrip()

    def cp(self, src, dest) -> str:
        """
        Run 'cp' or 'scp' command for the specified source and destination
        arguments and return its output as a string.
        In case of `scp` source is always the remote.
        """
        # pylint: disable = invalid-name
        if self._remote is not None:
            return self.scp(src, dest).stdout.rstrip()

        return self.exec(['cp', '-urf', src, dest]).stdout.rstrip()

    def rm(self, args) -> str:
        """
        Run 'rm' command with the specified argument and return its output as a string.
        """
        # pylint: disable = invalid-name
        return self.exec(['rm'] + args).stdout.rstrip()

    def ls(self, args) -> str:
        """
        Run 'ls' command with the specified argument and return its output as a string.
        """
        # pylint: disable = invalid-name
        return self.exec(['ls'] + args).stdout.rstrip()

    def home(self) -> str:
        """
        Run 'echo' command with the specified argument and return its output as a string.
        """
        result = self.exec(['env'])

        env_list = result.stdout.splitlines()

        for env in env_list:
            if env.startswith('HOME'):
                return env.split("=")[1]

        return '/root'

    def store_pid(self, pid, pid_file):
        """
        Run 'echo' command with the specified argument and return its output as a string.
        """
        self.exec(['rm', '-f', str(pid_file)])
        self.exec(['touch', str(pid_file)])
        pid_s = f'BEGINFILE{{print"{pid}"}} 1'
        if self._remote:
            pid_s = f'\'{pid_s}\''
        self.exec(['awk', '-i', 'inplace', pid_s, str(pid_file)])

    def cat(self, arg) -> str:
        """
        Run 'cat' command with the specified argument and return its output as a string.
        """
        return self.exec(['cat', str(arg)]).stdout.rstrip()

    def mkdir(self, arg) -> None:
        """
        Execute 'mkdir -p' command with the specified argument to create
        directories if they don't exist.
        """
        self.exec(['mkdir', '-p', str(arg)])

    def exists(self, arg) -> int:
        """
        Run a conditional execution of 'file test' on the specified argument
        and return its exit code (0 for true).
        """
        return self.exec(['[', '-f', str(arg), '-o', '-d', str(arg), ']']).returncode == 0
