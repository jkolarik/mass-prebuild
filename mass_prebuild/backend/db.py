""" Store mass rebuild and package state

    This module provides an interface towards an sqlite3 database which stores
    information about ongoing build and their packages.
"""

from datetime import date
from pathlib import Path
import shutil
import sqlite3
from threading import Lock
from filelock import FileLock
import yaml

authorized_status_base = {
    'RUNNING',
    'SUCCESS',
    'CHECK_NEEDED',
    'FAILED',
    'UNCONFIRMED',
}
authorized_status = authorized_status_base | {
    'FREE',
    'PENDING',
    'COMPLETED',
}
authorized_collect = authorized_status_base | {'LOG'}

authorized_types = {'distgit', 'git', 'file', 'url', 'script'}
authorized_archs = {
    'all',
    'aarch64',
    'i386',
    'i586',
    'i686',
    'ppc64le',
    's390x',
    'x86',
    'x86_64',
}
all_archs = {'aarch64', 'ppc64le', 's390x', 'x86', 'x86_64'}


class MpbDb:
    """Mass pre-build database handler

        Provides helpers to manipulate the mass pre-builder database

    Attributes:
        con: The connection to the sqlite3 database
        path: The path used to create this database
        verbose: The level of verbosity

    """
    # pylint: disable = too-many-instance-attributes

    def __init__(self, path='/tmp/mpb/mpb.db', verbose=0):
        """Initialize mass pre-builder database

            Connection to the database is established, and tables are created
            if they don't exist.

        Args:
            path: A string giving the path to the database to use (it may not
            exist)
            verbose: The level of verbosity

        """
        path = Path(path)

        path.parent.mkdir(parents=True, exist_ok=True)

        self.verbose = verbose

        self.path = str(path)

        if self.verbose >= 2:
            print(f'Creating: {self.path}')

        # Serialize write operations for multi-thread and multi-process support
        self._process_lock = FileLock(self.path + ".lock")
        self._thread_lock = Lock()

        self.con = sqlite3.connect(self.path, check_same_thread=False)
        self.con.isolation_level = None
        self.con.row_factory = sqlite3.Row

        self._update_table = [
                self._update_tables_v0,
                self._update_tables_v1,
                self._update_tables_v2,
                self._update_tables_v3,
                self._update_tables_v4,
                self._update_tables_v5,
                ]

        self.init_cursor = self.con.execute("""PRAGMA user_version""")
        user_version = self.init_cursor.fetchone()[0]

        if user_version > len(self._update_table):
            print(f'Database version {user_version} is unsupported')
            print(f'Supported versions: <{len(self._update_table)}')
            raise ValueError(f'Incompatible DB {self.path}')

        for version, func in enumerate(self._update_table):
            if version < user_version:
                continue

            func()
            self.init_cursor.execute(f"""PRAGMA user_version={version + 1:d}""")
            self.con.commit()

            if self.verbose > 2:
                print(f'Migrated DB to version {version + 1}')

        self.init_cursor.close()
        # This cursor shouldn't be used any more.
        # New cursor needs to be created and destroyed on each function calls.
        self.init_cursor = None

        if self.verbose >= 4:
            print(f'Created {self.__class__.__name__} instance at 0x{id(self):x}')

    def __repr__(self):
        """Printable representation"""
        return self.path

    def __str__(self):
        """Printable representation"""
        return self.path

    def _add_column(self, column, ctype, default, table):
        """Add a missing column in one of the tables"""
        self.init_cursor.execute(
            f"""
            SELECT count(*) > 0
                FROM pragma_table_info('{table}')
                    WHERE name='{column}';
            """
        )

        if not self.init_cursor.fetchone()[0] > 0:
            print("Adding new column to table")
            self.init_cursor.execute(
                f"""ALTER TABLE {table}
                        ADD {column} {ctype} NOT NULL DEFAULT {default};"""
            )

    def _update_tables_v0(self):
        """Create initial version of the database"""
        default_arch = {'x86_64'}

        self.init_cursor.execute(
            f"""CREATE TABLE IF NOT EXISTS build (
                        build_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                        base_build_id INTEGER DEFAULT 0 NOT NULL,
                        name TEXT NOT NULL DEFAULT 'undef',
                        stage INTEGER NOT NULL DEFAULT 0,
                        state TEXT NOT NULL DEFAULT 'FREE'
                            CHECK (state IN ({str(authorized_status).strip('{}')})),
                        archs TEXT DEFAULT '{yaml.dump(default_arch)}' NOT NULL,
                        chroot TEXT DEFAULT 'fedora-rawhide' NOT NULL,
                        data TEXT DEFAULT '' NOT NULL
                        );
                """
        )

        self.init_cursor.execute(
            f"""CREATE TABLE IF NOT EXISTS package (
                        pkg_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                        name TEXT NOT NULL,
                        base_build_id INTEGER DEFAULT 0 NOT NULL,
                        build_id INTEGER DEFAULT 0 NOT NULL,
                        build_status TEXT
                            CHECK (build_status IN ({str(authorized_status).strip('{}')})),
                        pkg_type INTEGERS DEFAULT 0
                            CHECK (pkg_type IN (0, 1)),
                        archs TEXT DEFAULT '{yaml.dump(default_arch)}' NOT NULL,
                        skip_archs TEXT DEFAULT '{yaml.dump(default_arch)}' NOT NULL,
                        src_type TEXT DEFAULT 'distgit' NOT NULL
                            CHECK (src_type IN ({str(authorized_types).strip('{}')})),
                        src TEXT DEFAULT '',
                        committish TEXT DEFAULT '',
                        priority INTEGER DEFAULT 0 NOT NULL,
                        after_pkg INTEGER DEFAULT 0 NOT NULL,
                        with_pkg INTEGER DEFAULT 0 NOT NULL
                        );
                """
        )

    def _update_tables_v1(self):
        """Database v1"""
        self._add_column('enable_priorities', 'BOOLEAN', 1, 'build')
        self._add_column('dnf_conf', 'TEXT', '""', 'build')
        default = yaml.dump(['fail'])
        self._add_column('collect', 'TEXT', f'"{default}"', 'build')
        default = yaml.dump([])
        self._add_column('config', 'TEXT', f'"{default}"', 'build')

    def _update_tables_v2(self):
        default_arch = {'x86_64'}

        self.con.commit()
        self.con.close()

        ext = f'v1.{date.today()}'
        path = Path(f'{self.path}.{ext}')
        if not path.exists():
            print(f'Saving database to {self.path}.{ext} as update may be destructive')
            shutil.copy2(self.path, f'{self.path}.{ext}')

        self.con = sqlite3.connect(self.path, check_same_thread=False)
        self.con.isolation_level = None
        self.con.row_factory = sqlite3.Row

        # Backward compatibility: Remove checks to ease table modifications
        self.init_cursor = self.con.execute(
            f"""CREATE TABLE IF NOT EXISTS package_new (
                        pkg_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                        name TEXT NOT NULL,
                        base_build_id INTEGER DEFAULT 0 NOT NULL,
                        build_id INTEGER DEFAULT 0 NOT NULL,
                        build_status TEXT,
                        pkg_type INTEGERS DEFAULT 1,
                        archs TEXT DEFAULT '{yaml.dump(default_arch)}' NOT NULL,
                        skip_archs TEXT DEFAULT '{yaml.dump(default_arch)}' NOT NULL,
                        src_type TEXT DEFAULT 'distgit' NOT NULL,
                        src TEXT DEFAULT '',
                        committish TEXT DEFAULT '',
                        priority INTEGER DEFAULT 0 NOT NULL,
                        after_pkg INTEGER DEFAULT 0 NOT NULL,
                        with_pkg INTEGER DEFAULT 0 NOT NULL
                        );
                """
        )
        self.init_cursor.execute("""INSERT INTO package_new SELECT * FROM package;""")

        self.init_cursor.execute("""SELECT name, pkg_id, pkg_type FROM package;""")
        pkgs = self.init_cursor.fetchall()

        for pkg in pkgs:
            print(f'Updating database entry for: {pkg["name"]:100}', end='\r')
            self.init_cursor.execute(
                    """UPDATE package_new
                            SET pkg_type = ?
                                WHERE pkg_id = ?;
                    """,
                    (1 << pkg['pkg_type'], pkg['pkg_id'])
                    )

        self.init_cursor.execute("""DROP TABLE package;""")
        self.init_cursor.execute("""ALTER TABLE package_new RENAME TO package;""")

    def _update_tables_v3(self):
        """Database v3"""
        self._add_column('skip_archs', 'TEXT', f'"{yaml.dump(set())}"', 'build')
        self._add_column('retry', 'INTEGER', 0, 'build')
        self._add_column('retry', 'INTEGER', 0, 'package')
        self._add_column('retry_count', 'INTEGER', 0, 'package')

    def _update_tables_v4(self):
        """Database v4"""
        self._add_column('nvr', 'TEXT', '""', 'package')

    def _update_tables_v5(self):
        """Database v5"""
        self._add_column('backend_failures', 'INTEGER', 0, 'package')

    def release(self):
        """Destroy the mas pre-builder database object

        All pending actions are committed to the database, and the
        connection is closed.
        """
        if self.verbose >= 4:
            print(f'Destroyed {self.__class__.__name__} instance at 0x{id(self):x}')
        self.con.commit()
        self.con.close()

    @property
    def verbose(self):
        """Return current verbosity level"""
        return self._verbose

    @verbose.setter
    def verbose(self, value):
        self._verbose = max(value, 0)

    def get_stage(self, build_id):
        """Retrieve last executed stage from DB

        Args:
            build_id: ID of the mass pre-build to get the stage of

        Returns:
            An integer

        """
        cur = self.con.execute(
            """SELECT stage FROM build WHERE build_id = ?""", (build_id,)
        )
        exists = cur.fetchone()
        cur.close()

        if exists is not None:
            return exists[0]

        print(f'Build ID {build_id} not found.')
        return 0

    def set_stage(self, build_id, new_stage):
        """Set the current stage for a given mass pre-build

        Args:
            build_id: ID of the mass pre-build to set the stage of
            new_stage: The value of the stage to set

        """
        with self._process_lock, self._thread_lock:
            cur = self.con.execute(
                """SELECT * FROM build WHERE build_id = ?""", (build_id,)
            )
            exists = cur.fetchall()
            cur.close()

            if len(exists):
                cur = self.con.execute(
                    """UPDATE build
                            SET stage = ?
                                WHERE build_id = ?
                    """,
                    (new_stage, build_id),
                )
                cur.close()
            else:
                print(f'Build ID {build_id} not found.')

    def get_state(self, build_id):
        """Get the state of the build

        Args:
            build_id: ID of the mass pre-build to get the state of

        Returns:
            A string representing the current state

        """
        with self._process_lock, self._thread_lock:
            cur = self.con.execute(
                """SELECT state FROM build WHERE build_id = ?""", (build_id,)
            )
            exists = cur.fetchone()
            cur.close()

        if exists is not None:
            return exists[0]

        print(f'Build ID {build_id} not found.')
        return 'FREE'

    def set_state(self, build_id, new_state):
        """Set the new state of a mass pre-build build.

        Args:
            build_id:ID of the mass pre-build to set the state of
            new_state: A string representing the new state

        Raises:
            ValueError: Given string is forbidden or ID doesn't exists

        """
        if new_state not in authorized_status:
            raise ValueError(f'Unauthorized build state {new_state}.')

        with self._process_lock, self._thread_lock:
            cur = self.con.execute(
                """SELECT * FROM build WHERE build_id = ?""", (build_id,)
            )
            exists = cur.fetchall()
            cur.close()

            if not exists:
                raise ValueError(f'Build ID {build_id} not found.')

            cur = self.con.execute(
                """UPDATE build
                        SET state = ?
                            WHERE build_id = ?
                """,
                (new_state, build_id),
            )
            cur.close()

    def set_name(self, build_id, new_name):
        """Set the new name of a mass pre-build build.

        Args:
            build_id: ID of the mass pre-build to set the name of
            new_name: A string representing the new name

        Raises:
            ValueError: ID doesn't exists

        """
        cur = self.con.execute(
            """SELECT * FROM build WHERE build_id = ?""", (build_id,)
        )
        exists = cur.fetchall()
        cur.close()

        if not exists:
            raise ValueError(f'Build ID {build_id} not found.')

        with self._process_lock, self._thread_lock:
            cur = self.con.execute(
                """UPDATE build
                        SET name = ?
                            WHERE build_id = ?
                """,
                (new_name, build_id),
            )
            cur.close()

    def remove(self, build_id):
        """Remove a build and the associated checker and packages from the database"""
        with self._process_lock, self._thread_lock:
            cursor = self.con.cursor()

            checker = self.build_by_base_id_unlocked(build_id)
            if checker:
                cursor.execute("""DELETE FROM package WHERE base_build_id = ?""",
                               (checker['build_id'],))

            cursor.execute("""DELETE FROM package WHERE base_build_id = ?""", (build_id,))
            cursor.execute("""DELETE FROM build WHERE base_build_id = ?""", (build_id,))

            cursor.execute("""DELETE FROM build WHERE build_id = ?""", (build_id,))

            self.con.commit()

            cursor.close()

    def new_build(self, config):
        """Creates a new entry for mass pre-build in the database

        Args:
            config: Configuration of the new entry

        Returns:
            An integer, the ID of the new entry to be used for later reference

        """
        build_id = 0
        with self._process_lock, self._thread_lock:
            cursor = self.con.cursor()
            db_save = config['database']

            if not isinstance(db_save, str):
                config['database'] = str(config['database'])

            cursor.execute(
                """INSERT INTO build (state, base_build_id, archs, chroot,
                                      data, enable_priorities, dnf_conf,
                                      collect, config, retry, skip_archs)
                        VALUES ('FREE', ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                """,
                (
                    config['base_build_id'],
                    yaml.dump(config['archs']),
                    config['chroot'],
                    config['data'],
                    config['enable_priorities'],
                    config['dnf_conf'],
                    yaml.dump(config['collect_list']),
                    yaml.dump(config),
                    config['retry'],
                    yaml.dump(config['skip_archs']),
                ),
            )
            self.con.commit()

            # Restore the database
            config['database'] = db_save

            build_id = cursor.lastrowid

            if 'name' in config:
                name = config['name']
            else:
                name = f'mpb.{build_id}'

            cursor.execute(
                """UPDATE build
                        SET name = ?
                            WHERE build_id = ?
                """,
                (name, build_id),
            )
            self.con.commit()

            cursor.close()
        return build_id

    def save_config(self, build_id, config):
        """Save the provided config data into the build table"""
        if any([not build_id, not config]):
            return

        with self._process_lock, self._thread_lock:
            cursor = self.con.cursor()
            cursor.execute(
                """ UPDATE build
                        SET config = ?
                        WHERE build_id = ?
                """,
                (
                    yaml.dump(config),
                    build_id,
                ),
            )
            self.con.commit()
            cursor.close()

    def update_package(self, pkg_data):
        """Update a package entry or create one if it doesn't exist

        Args:
            pkg_data: The data of the package to be updated or created

        Returns:
            An integer representing the ID of the package for later reference

        """
        with self._process_lock, self._thread_lock:
            cursor = self.con.cursor()
            if pkg_data['pkg_id']:
                cursor.execute(
                    """ REPLACE INTO package (pkg_id, name, base_build_id, build_id,
                                              build_status, pkg_type, archs, skip_archs,
                                              src_type, src, committish, priority, after_pkg,
                                              with_pkg, retry, retry_count, nvr)
                                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                    """,
                    (
                        pkg_data['pkg_id'],
                        pkg_data['name'],
                        pkg_data['base_build_id'],
                        pkg_data['build_id'],
                        pkg_data['build_status'],
                        pkg_data['pkg_type'],
                        yaml.dump(pkg_data['archs']),
                        yaml.dump(pkg_data['skip_archs']),
                        pkg_data['src_type'],
                        pkg_data['src'],
                        pkg_data['committish'],
                        pkg_data['priority'],
                        pkg_data['after_pkg'],
                        pkg_data['with_pkg'],
                        pkg_data['retry'],
                        pkg_data['retry_count'],
                        pkg_data['nvr'],
                    ),
                )
                self.con.commit()
                cursor.close()
                return pkg_data['pkg_id']

            cursor.execute(
                """INSERT INTO package (name, base_build_id, build_id, build_status,
                                        pkg_type, archs, skip_archs, src_type, src,
                                        committish, priority, after_pkg, with_pkg,
                                        retry, retry_count, nvr)
                        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                """,
                (
                    pkg_data['name'],
                    pkg_data['base_build_id'],
                    pkg_data['build_id'],
                    pkg_data['build_status'],
                    pkg_data['pkg_type'],
                    yaml.dump(pkg_data['archs']),
                    yaml.dump(pkg_data['skip_archs']),
                    pkg_data['src_type'],
                    pkg_data['src'],
                    pkg_data['committish'],
                    pkg_data['priority'],
                    pkg_data['after_pkg'],
                    pkg_data['with_pkg'],
                    pkg_data['retry'],
                    pkg_data['retry_count'],
                    pkg_data['nvr'],
                ),
            )
            self.con.commit()
            new_id = cursor.lastrowid
            cursor.close()
            return new_id

    def builds(self):
        """Return all available builds"""
        with self._process_lock, self._thread_lock:
            cursor = self.con.execute("""SELECT * FROM build ORDER BY build_id ASC;""")
            builds = cursor.fetchall()
            cursor.close()

        return builds

    def build_by_id(self, build_id):
        """Return an sqliteRow containing the build data"""
        with self._process_lock, self._thread_lock:
            cursor = self.con.execute(
                """SELECT * FROM build
                        WHERE build_id = ?
                """,
                (build_id,),
            )
            build = cursor.fetchone()
            cursor.close()
        return build

    def build_by_base_id_unlocked(self, base_build_id):
        """Return a list of sqliteRow containing build data"""
        cursor = self.con.execute(
            """SELECT * FROM build
                    WHERE base_build_id = ?
            """,
            (base_build_id,),
        )
        build = cursor.fetchone()
        cursor.close()
        return build

    def build_by_base_id(self, base_build_id):
        """Return a list of sqliteRow containing build data"""
        with self._process_lock, self._thread_lock:
            return self.build_by_base_id_unlocked(base_build_id)

    def packages_by_base_build_id(self, base_build_id):
        """Return a list of sqliteRow containing package data"""
        with self._process_lock, self._thread_lock:
            return self.con.execute(
                """SELECT pkg_id, name FROM package
                        WHERE base_build_id = ?
                """,
                (base_build_id,),
            ).fetchall()

    def package_by_id(self, pkg_id):
        """Return a sqliteRow containing package data"""
        with self._process_lock, self._thread_lock:
            cursor = self.con.execute(
                """SELECT * FROM package
                        WHERE pkg_id = ?
                """,
                (pkg_id,),
            )
            ret = cursor.fetchone()
            cursor.close()
        return ret
