# pylint: disable = missing-module-docstring duplicate-code exec-used
import setuptools
from setuptools_scm import get_version

try:
    VERSION = get_version(root='.', relative_to=__file__)
except LookupError:
    with open("mass_prebuild/__init__.py", 'r', encoding='utf-8') as file:
        exec(file.read())

setuptools.setup(version=VERSION)
