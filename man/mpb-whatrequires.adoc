= mpb-whatrequires (1)
:doctype: manpage
:manmanual: mpb-whatrequires

== Name

mpb-whatrequires - generate a list of reverse dependencies for package names given as input

== Synopsis

*mpb-whatrequires* [_OPTION_] _package1_ _package2_...

== Description

Given a list of packages as input, *mpb-whatrequires* will give the latest version of these packages, and may compute a list of packages that depend on them, either at build time or at runtime.

This tool is the command line interface to the functions that are used by *mpb* (1) in order to compute reverse dependencies.
Internally, it retrieves information using DNF and Koji APIs.

// tag::options[]
== Options

*-h, --help*::
  Show the help message and exit.

*--verbose, -V*::
  Increase output verbosity.
  This option may be provided multiple times to modify the amount of information provided (e.g. *-VV*).
  Other options are affected by the verbosity level, and may provide different information depending on it.

*--version, -v*::
  Show the program version and exit.

*--output, -o* _OUTPUT_::
  Where to store the list of packages

*--distrib* _DISTRIB_::
  Distribution to be considered for the package repository.
  By default, *mpb-whatrequires* looks at Fedora Rawhide.

*--releasever, -r* _RELEASEVER_::
  Release version for the given distribution (e.g 36, rawhide ...).

*--arch, -a* _ARCH_::
  The architecture to filter the packages for.
  Note that _noarch_ packages are always included.
  By default, *x86_64* packages are retrieved.

*--depth, -d* _DEPTH_::
  The dependency depth, i.e. for _DEPTH_=1, considering the following dependencies:
  D -> C -> B -> A
  Then C and B are retrieved, when A is given as input.
  With _DEPTH_=2 then D would also be given.

*--repo, r* _REPO_::
  A custom dnf configuration in a special format, see *mpb.config*(5) for more details on how to write this configuration file.
  This may be used in order to provide repositories information on top of the built-in ones, or in order to overwrite them with different values.
  +
  A use case is for example to fake Rawhide repositories with ELN ones, so that a reduced set of package corresponding to the ones available in ELN is built in a rawhide environment.

*--no-deps*::
  Retrieve last build info for the given packages, but don't compute the dependency list.

*--no-priority*::
  When computing the dependency list, disable priority calculation.

// end::options[]

== Resources

More information about the mass-prebuilder can be found at https://gitlab.com/fedora/packager-tools/mass-prebuild.

== See also

*mass-prebuild*(7), *mpb*(1), *mpb.config*(5)
