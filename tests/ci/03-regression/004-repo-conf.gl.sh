echo -e "${BBlue}##### Checking repo configuration feature #####${Color_Off}"

DATE=$(date)
echo -e "${Purple}Starting date: ${DATE}${Color_Off}"

echo -e "${Blue}Ensuring that we don't get ELN package while looking for Rawhide${Color_Off}"
mpb-whatrequires kernel --no-deps > ${LOG_FILE} 2>&1
if [ ! "x$(grep eln ${LOG_FILE} || true)" == "x" ]
then
  mpb-whatrequires kernel --no-deps
  FAIL
fi

cat > /tmp/.mpb/eln_on_rawhide.conf.yaml << EOF
# This fake configuration allows to search reverse dependencies
# in ELN repositories for a rawhide build
fedora:
  rawhide:
    base: https://odcs.fedoraproject.org/composes/production/latest-Fedora-ELN/compose/
    bin-repos:
    - BaseOS/{arch}/os/
    koji: https://koji.fedoraproject.org/kojihub
    src-repos:
    - BaseOS/source/tree/
EOF

echo -e "${Blue}Ensuring that we DO get ELN package while looking for Rawhide with fake repos${Color_Off}"
mpb-whatrequires kernel -R /tmp/.mpb/eln_on_rawhide.conf.yaml --no-deps > ${LOG_FILE} 2>&1
if [ "x$(grep eln ${LOG_FILE} || true)" == "x" ]
then
  mpb-whatrequires kernel --no-deps -R /tmp/.mpb/eln_on_rawhide.conf.yaml || true
  FAIL
fi

echo -e "${BGreen}OK${Color_Off}"

echo ""
